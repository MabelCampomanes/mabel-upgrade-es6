//// Iteración #5: Filter
//
//// 5.1 Dado el siguiente array, utiliza .filter() para generar un nuevo array 
//// con los valores que sean mayor que 18
//     var ages = [22, 14, 24, 55, 65, 21, 12, 13, 90];
//
//     var eighteen = ages.filter((age) =>  age>= 18);
//     
//     console.log(eighteen);
//
//// 5.2 Dado el siguiente array, utiliza .filter() para generar un nuevo array 
//// con los valores que sean par.
//    var ages = [22, 14, 24, 55, 65, 21, 12, 13, 90];
//
//    var par = ages.filter((age) => {
//        return age % 2 == 0;
//        });
//
//        console.log(par);
//
//
//// 5.3 Dado el siguiente array, utiliza .filter() para generar un nuevo array 
//// con los streamers que tengan el gameMorePlayed = 'League of Legends'.
//    var streamers = [
// 	    {name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
// 	    {name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'}, 
// 	    {name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
// 	    {name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
//    ];
//
//    const mostPlayed = streamers.filter(streamer => streamer.gameMorePlayed === 'League of Legends');
//  
//    console.log(mostPlayed);
// 
//// 5.4 Dado el siguiente array, utiliza .filter() para generar un nuevo array 
//// con los streamers que incluyan el caracter 'u' en su propiedad .name. Recomendamos 
//// usar la funcion .includes() para la comprobación.
//    var streamers = [
//    	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
//    	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'},
//    	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
//    	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
//    ];
//    
//    const uNames = streamers.filter((streamer) => {
//        return streamer.name.includes('u');
//    })
//
//    console.log(uNames);
//
// 5.5 utiliza .filter() para generar un nuevo array con los streamers que incluyan 
// el caracter 'Legends' en su propiedad .gameMorePlayed. Recomendamos usar la funcion 
// .includes() para la comprobación.
// Además, pon el valor de la propiedad .gameMorePlayed a MAYUSCULAS cuando 
// .age sea mayor que 35.
//
//    const sLegends = streamers.filter((streamer) => {
//        if(streamer.age > 35){
//            streamer.gameMorePlayed = streamer.gameMorePlayed.toUpperCase();
//        }
//    return !streamer.gameMorePlayed.includes('Legend');
//})    
//
// 5.6 Dado el siguiente html y javascript, utiliza .filter() para mostrar por consola 
// los streamers que incluyan la palabra introducida en el input. De esta forma, si 
// introduzco 'Ru' me deberia de mostrar solo el streamer 'Rubius'. Si
// introduzco 'i', me deberia de mostrar el streamer 'Rubius' e 'Ibai'.
    const streamers2 = [
    	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
    	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'},
    	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
    	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
    ];

    const onFilterStreamers = (event) => {
        const result = streamers2.filter((item) => item.name.toLocaleLowerCase().includes(event.target.value.toLocaleLowerCase())); 
        console.log(result);
    }

    const inputDom = document.getElementById('input-text');
    inputDom.addEventListener('input', onFilterStreamers);




// 
// 5.7 Dado el siguiente html y javascript, utiliza .filter() para mostrar por consola 
// los streamers que incluyan la palabra introducida en el input. De esta forma, si 
// introduzco 'Ru' me deberia de mostrar solo el streamer 'Rubius'. Si introduzco 'i', 
// me deberia de mostrar el streamer 'Rubius' e 'Ibai'.
// En este caso, muestra solo los streamers filtrados cuando hagamos click en el button.
    const streamers3 = [
    	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
    	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'},
    	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
    	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
    ];
