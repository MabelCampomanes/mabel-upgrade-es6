'use strict'

// 4.1 Dado el siguiente array, devuelve un array con sus nombres 
//  utilizando .map().
    const users = [
	    {id: 1, name: 'Abel'},
	    {id:2, name: 'Julia'},
	    {id:3, name: 'Pedro'},
	    {id:4, name: 'Amanda'}
    ];

    const result = users.map((nombre) => nombre.name );

    console.log(result);
//  
//  4.2 Dado el siguiente array, devuelve una lista que contenga los valores 
//  de la propiedad .name y cambia el nombre a 'Anacleto' en caso de que 
//  empiece por 'A'.
    const users2 = [
     	{id: 1, name: 'Abel'},
     	{id:2, name: 'Julia'},
     	{id:3, name: 'Pedro'},
     	{id:4, name: 'Amanda'}
     ];

    const lista = users2.map((nombres) => {
        if (nombres.name[0].includes('A')) {
            nombres.name= 'Anacleto';
        }
        return nombres.name;
    });

    console.log(lista);

//  4.3 Dado el siguiente array, devuelve una lista que contenga los valores 
//  de la propiedad .name y añade al valor de .name el string ' (Visitado)' 
//  cuando el valor de la propiedad isVisited = true.
    const cities = [
     	{isVisited:true, name: 'Tokyo'}, 
     	{isVisited:false, name: 'Madagascar'},
     	{isVisited:true, name: 'Amsterdam'}, 
     	{isVisited:false, name: 'Seul'}
    ];

    const visited = cities.map((city) => {
        if(city.isVisited === true){
            city.name += " Visitado";
        }
        return city.name;
    })

    console.log(visited);
